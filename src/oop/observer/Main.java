package oop.observer;

public class Main {
    public static void main(String[] args) {
        WeatherData wd = new WeatherData();

        CurrentConditionDisplay ccd = new CurrentConditionDisplay(wd);

        wd.setMeasurements(12,12,31);
        wd.setMeasurements(14, 24, 41);
    }
}
