package oop.observer;

public class StatisticsDisplay implements Observer,DisplayElement {
    private float avg;
    private float min;
    private float max;
    private int count;
    private Subject wd;

    public StatisticsDisplay(Subject wd){
        this.count=0;
        this.min=0;
        this.max=0;
        this.avg=0;
        this.wd=wd;
        this.wd.registerObserver(this);
    }

    public void update(float temperature, float humidity, float pressure){
        if (this.min>temperature){
            this.min=temperature;
        } else if (this.max<temperature){
            this.max=temperature;
        }
        updateAvg(temperature);
    }

    public void updateAvg(float temp){
        avg=avg*count;
    }

    public void display(){}
}
