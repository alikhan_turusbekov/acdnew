package oop.observer;

public interface DisplayElement {
    public void display();
}
