package oop.week1;

public class Person {
    private String name;
    private int birthYear;

    public Person(){
    }

    public Person(String name,int birthYear){
        this.name=name;
        this.birthYear=birthYear;
    }

    public String getName() {
        return name;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public int age(){
        return 2020-this.birthYear;
    }

    public void input(String name, int birthYear){
        this.birthYear=birthYear;
        this.name=name;
    }

    public void output(){
        System.out.println("Name—"+this.name+" age—"+this.age());
    }

    public void changeName(String name){
        this.name=name;
    }
}
