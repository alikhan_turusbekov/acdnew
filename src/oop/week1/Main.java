package oop.week1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        /*Student*/
        Student s1 = new Student();
        Student s2 = new Student();
        Student s3 = new Student("cheburek", 2.50);
        s2.setName("Kek"); s2.changeRating(3.0);
        s1.setName("Lol"); s1.changeRating(2.0);

        System.out.println("Average rating — "+ Student.getAvgRating());
        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);

        /*Employee*/
        Employee e1 = new Employee("a", 1.0, 200);
        Employee e2 = new Employee("b", 4.5, 50);
        Employee e3 = new Employee("c", 2.5, 50);

        System.out.println();
        System.out.println("Total hours — " + Employee.getTotalSum());

        /*Person*/
        Person p[] = new Person[5];
        for(int i=0; i<5;i++){
            p[i]= new Person();
        }

        for(int i=0; i<5;i++){
            String name = s.next();
            int year = s.nextInt();
            p[i].input(name, year);
        }
    }
}
