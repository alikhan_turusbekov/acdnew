package oop.week1;

public class Employee {
    private String name;
    private double rate;
    private int hours;
    private static int totalSum;

    public String getName() {
        return name;
    }

    public double getRate() {
        return rate;
    }

    public int getHours() {
        return hours;
    }

    public static int getTotalSum() {
        return totalSum;
    }

    public Employee(){
        this.name=null;
        this.rate=0;
        this.hours=0;
    }

    public Employee(String name){
        this.name=name;
        this.rate=0;
        this.hours=0;
    }

    public Employee(String name, double rate){
        this.name=name;
        this.rate=rate;
        this.hours=0;
    }

    public Employee(String name, double rate, int hours){
        this.name=name;
        this.rate=rate;
        this.hours=hours;
        totalSum+=hours;
    }

    public int salary(){
        return (int) (rate*hours);
    }

    public void changeRate(double rate){
        this.rate=rate;
    }

    public int bonuses(){
        return (int) (0.1*salary());
    }

    @Override
    public String toString() {
        return "Employee info: Name—"+this.name+" Rate—"+this.rate+" Hours—"+this.hours;
    }
}

