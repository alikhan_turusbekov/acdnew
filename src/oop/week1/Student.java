package oop.week1;

public class Student{
    private String name;
    private double rating;
    private static double avgRating=0;

    private static int count=0;

    public Student(){
        count++;
    }

    public Student(String name, double rating){
        this.name=name;
        this.rating=rating;
        avgRating+=rating;
        count++;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void changeRating(double rating) {
        avgRating -= this.rating;
        this.rating = rating;
        avgRating += rating;
    }

    public int getCount() {
        return count;
    }

    public String getName() {
        return name;
    }

    public double getRating() {
        return rating;
    }

    public static double getAvgRating() {
        return avgRating/count;
    }

    @Override
    public String toString() {
        return "Student's name:"+this.name+", rating:"+this.rating;
    }
}

