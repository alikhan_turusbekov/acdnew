package oop.expection;

public class CharacterIsAlreadyDeadException extends Exception {
    public CharacterIsAlreadyDeadException(String s){
        super(s);
    }
}
