package oop.expection;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {
    public static void main(String[] args) throws IOException {
        Character c = new Character("Alikhan");
        c.beingAttacked();
        c.beingAttacked();

        File file = new File("/Users/User/Desktop/characters.txt");
        FileWriter fileWriter = new FileWriter(file, true);
        PrintWriter printWriter = new PrintWriter(fileWriter);

        try {
            c.attack();
        } catch (CharacterIsAlreadyDeadException e) {
            e.printStackTrace();
            printWriter.println(e.getMessage());
        } finally {
            c.healingPotion();
            fileWriter.close();
            printWriter.close();
        }
    }
}
