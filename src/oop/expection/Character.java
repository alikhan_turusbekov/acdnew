package oop.expection;

public class Character {
    private int health;
    private String name;
    private String weapon;

    public Character(String name) {
        this.name = name;
        this.weapon = "sword";
        this.health = 100;
    }

    public void attack() throws CharacterIsAlreadyDeadException{
        if (!isDead()){
            System.out.println(this.name+" is attacking with "+ this.weapon);
        } else {
            throw new CharacterIsAlreadyDeadException("Character named "+ this.name + " is already killed!");
        }
    }

    public void healingPotion(){
        if (this.health == 0) {
            System.out.println(this.name+ " was REVIVED!");
        } else if (this.health < 100){
            this.health+=10;
            System.out.println(this.name + "has taken healing potion and now his/her health is "+this.health);
        }
    }

    public void beingAttacked(){
        if (!isDead()) {
            this.health-=50;
            System.out.println(this.name+" was attacked and his/her health is now "+this.health);
        }
    }

    public boolean isDead(){
        if (health<=0){
            return true;
        }
        return false;
    }


}
