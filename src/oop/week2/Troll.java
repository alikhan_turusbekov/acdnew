package oop.week2;

import oop.week2.Weapons.AxeBehavior;

public class Troll extends Character {
    public Troll(){
        setName("TROLL WITH NO NAME");
        setWb(new AxeBehavior());
        setHealth(30);
    }

    public Troll(String name){
        name = name.toUpperCase();
        this.setName("TROLL "+name);
        setWb(new AxeBehavior());
        setHealth(30);
    }
}
