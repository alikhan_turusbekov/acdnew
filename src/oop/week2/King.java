package oop.week2;

import oop.week2.Weapons.KnifeBehavior;

public class King extends Character {
    public King(){
        this.setName("KING WITH NO NAME");
        setWb(new KnifeBehavior());
        setHealth(100);
    }

    public King(String name){
        name = name.toUpperCase();
        this.setName("KING "+name);
        setWb(new KnifeBehavior());
        setHealth(100);
    }
}
