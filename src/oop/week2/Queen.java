package oop.week2;

import oop.week2.Weapons.BowAndArrowBehavior;

public class Queen extends Character{
    public Queen(){
        setName("QUEEN WITH NO NAME");
        setWb(new BowAndArrowBehavior());
        setHealth(70);
    }

    public Queen(String name){
        name = name.toUpperCase();
        setName("QUEEN "+name);
        setWb(new BowAndArrowBehavior());
        setHealth(70);
    }
}
