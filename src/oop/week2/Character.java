package oop.week2;

import oop.week2.Weapons.*;

public abstract class Character {
    private String name;
    private int Health;
    private WeaponBehavior wb;
    private Character target = null;

    public void fight(){
        if (target == null) {
            System.out.println("No target has been chosen for the " + this.getName() + "!");
            return;
        }
        if (target.getHealth()<=0){
            System.out.println( this.target.getName() + " is already DEAD!");
            return;
        }
        if (this.wb.attack(this.target.getHealth())<=0){
            System.out.print(this.name + " KILLED "+ target.getName() +" using ");
            this.wb.useWeapon();
            System.out.println();
            target.setHealth(this.wb.attack(target.getHealth()));
            return;
        }
        System.out.print(this.name + " just did the ");
        this.wb.useWeapon();
        System.out.print(" on "+target.getName()+". ");
        System.out.print("The health of " + target.getName()+" was "+target.getHealth());
        target.setHealth(this.wb.attack(target.getHealth()));
        System.out.println(", but now it is "+target.getHealth());
    }

    public void changeWeapon(String nameWeapon) {
        if (nameWeapon == "AXE") {
            wb = new AxeBehavior();
        } else if (nameWeapon == "SWORD") {
            wb = new SwordBehavior();
        } else if (nameWeapon == "BOW") {
            wb = new BowAndArrowBehavior();
        } else if (nameWeapon == "KNIFE") {
            wb = new KnifeBehavior();
        }
    }

    public void setTarget(Character target){
        this.target=target;
    }

    public String getName() {
        return name;
    }

    public Character getTarget() {
        return target;
    }

    public int getHealth() {
        return Health;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWb(WeaponBehavior wb) {
        this.wb = wb;
    }

    public void setHealth(int health) {
        Health = health;
    }

    @Override
    public String toString() {
        return "Info about Character:" + "/n" +
                "Name: " + this.name + "/n" +
                "Health: " + this.getHealth() + "/n" +
                "Weapon: " + this.wb.getName() + "/n";
    }
}
