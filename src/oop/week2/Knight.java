package oop.week2;

import oop.week2.Weapons.SwordBehavior;

public class Knight extends Character {
    public Knight(){
        this.setName("KNIGHT WITH NO NAME");
        setWb(new SwordBehavior());
        setHealth(80);
    }

    public Knight(String name){
        name = name.toUpperCase();
        this.setName("KNIGHT "+name);
        setWb(new SwordBehavior());
        setHealth(80);
    }
}
