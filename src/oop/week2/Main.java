package oop.week2;

public class Main {
    public static void main(String[] args) {
        System.out.println(); // чтобы в терминале красиво было
        Queen q = new Queen("Elizavetta");
        King k = new King("Arthur");
        q.setTarget(k);
        k.setTarget(q);
        q.fight();
        k.fight();
        q.changeWeapon("AXE");
        k.changeWeapon("SWORD");
        q.fight();
        k.fight();
        q.fight();
        q.fight();
        q.fight();
        q.fight();
    }
}
