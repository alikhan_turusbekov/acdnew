package oop.week2.Weapons;

public class AxeBehavior implements WeaponBehavior {
    @Override
    public String getName(){
        return "AXE";
    }

    @Override
    public int attack(int health) {
        health-=30;
        if (health<=0) {
            return 0;
        }
        return health;
    }

    @Override
    public void useWeapon(){
        System.out.print("AXE ATTACK");
    }
}
