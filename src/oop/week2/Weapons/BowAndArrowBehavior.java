package oop.week2.Weapons;

public class BowAndArrowBehavior implements WeaponBehavior {
    @Override
    public String getName(){
        return "BOW";
    }

    @Override
    public int attack(int health) {
        health-=2;
        if (health<=0) {
            return 0;
        }
        return health;
    }

    @Override
    public void useWeapon(){
        System.out.print("ARROW ATTACK");
    }
}
