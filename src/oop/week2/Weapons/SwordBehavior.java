package oop.week2.Weapons;

public class SwordBehavior implements WeaponBehavior {
    @Override
    public String getName(){
        return "SWORD";
    }

    @Override
    public int attack(int health) {
        health-=5;
        if (health<=0) {
            return 0;
        }
        return health;
    }

    @Override
    public void useWeapon(){
        System.out.print("SWORD ATTACK");
    }
}
