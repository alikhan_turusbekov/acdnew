package oop.week2.Weapons;

public interface WeaponBehavior {
    String getName();
    void useWeapon();
    int attack(int health);
}
