package oop.week2.Weapons;

public class KnifeBehavior implements WeaponBehavior {
    @Override
    public String getName(){
        return "KNIFE";
    }

    @Override
    public int attack(int health) {
        health-=3;
        if (health<=0) {
            return 0;
        }
        return health;
    }

    @Override
    public void useWeapon(){
        System.out.print("KNIFE ATTACK");
    }
}
