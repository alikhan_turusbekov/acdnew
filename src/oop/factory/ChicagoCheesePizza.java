package oop.factory;

public class ChicagoCheesePizza extends Pizza {
    public ChicagoCheesePizza(){
        name = "Chicago Cheese Pizza";
        dough = "Extra Thick Crust Dough";
        sauce = "Plum Tomato Sauce";

        toppings.add("Shredded Mozzarella Cheese");
    }

    @Override
    void cut(){
        System.out.println("Cutting pizza into squares");
    }
}
