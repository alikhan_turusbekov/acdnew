package oop.factory;

public class ChicagoVeggiePizza extends Pizza{
    public ChicagoVeggiePizza(){
        name = "Chicago Veggie Pizza";
        dough = "Thick Crust Dough";
        sauce = "Kek Sauce";

        toppings.add("DAMN Veggie");
    }
}
