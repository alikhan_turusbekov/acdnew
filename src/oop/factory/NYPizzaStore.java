package oop.factory;

public class NYPizzaStore extends PizzaStore {
    @Override
    Pizza createPizza(String type) {
        switch(type){
            case "cheese": return new NYCheesePizza();
            case "veggie": return new NYVeggiePizza();
            default: return null;
        }
    }
}
