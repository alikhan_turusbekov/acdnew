package oop.factory;

public class NYVeggiePizza extends Pizza {
    public NYVeggiePizza(){
        name = "NY Veggie Pizza";
        dough = "Thin Crust Dough";
        sauce = "Mari Sauce";

        toppings.add("Grated Regianno Veggie");
    }
}
