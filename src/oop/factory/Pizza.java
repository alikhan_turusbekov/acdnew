package oop.factory;

import java.util.ArrayList;

public abstract class Pizza {
    String name;
    String dough;
    String sauce;
    ArrayList<String> toppings = new ArrayList<String>();

    void prepare(){
        System.out.println("Preparing " + name);
        System.out.println("Tossing dough..." + dough);
        System.out.println("Adding sause..." + sauce);
        System.out.println("Adding toppings: ");
        for (String topping : toppings) {
            System.out.println("    " + topping);
        }
    }

    void bake(){
        System.out.println("Baking for 25 minutes at 350");
    }

    void cut(){
        System.out.println("Cutting pizza into slices");
    }

    void box(){
        System.out.println("the pizza is boxed");
    }

    public String getName(){
        return name;
    }
}
