package oop.factory;

public class NYCheesePizza extends Pizza {
    public NYCheesePizza(){
        name = "NY Cheese Pizza";
        dough = "Thin Crust Dough";
        sauce = "Marinara Sauce";

        toppings.add("Grated Regianno Cheese");
    }
}
