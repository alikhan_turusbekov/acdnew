package oop.factory;

public class ChicagoPizzaStore extends PizzaStore{
    @Override
    Pizza createPizza(String type) {
        switch(type){
            case "cheese": return new ChicagoCheesePizza();
            case "veggie": return new NYVeggiePizza();
            default: return null;
        }
    }
}
