package acd.searching.bfsanddfs;

public class BinaryTree {
    public BinaryTreeNode root;

    public BinaryTree() {
        this.root = null;
    }

    public void insert(Integer data) {
        if (root == null) root = new BinaryTreeNode(data);
        else root.insert(data);
    }

    public BinaryTreeNode find(Integer data) {
        BinaryTreeNode node = findNode(this.root, data);
        if(node == null) return null;
        else return node;
    }

    private BinaryTreeNode findNode(BinaryTreeNode node, Integer key) {
        if(node == null) return null;
        if(key > node.data) return findNode(node.right, key);
        else if(key < node.data) return findNode(node.left, key);
        else return node;
    }
}
