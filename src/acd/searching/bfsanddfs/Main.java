package acd.searching.bfsanddfs;


public class Main {

    public static void printDFS(BinaryTree bt){
        if (bt.root==null) return;
        Stack s = new Stack();
        s.push(bt.root.data);
        while(!s.empty()){
            Node current = s.pop();
            if (current.data==null) return;
            System.out.print(current.data+" ");
            BinaryTreeNode node = bt.find(current.data);
            if (node.right!=null) s.push(node.right.data);
            if (node.left!=null) s.push(node.left.data);
        }
    }

    public static void printBFS(BinaryTree bt){
        if (bt.root==null) return;
        Queue q = new Queue();
        q.add(bt.root.data);
        while(!q.isempty()) {
            Node current = q.poll();
            System.out.print(current.data+" ");
            BinaryTreeNode node = bt.find(current.data);
            if (node.left!=null) q.add(node.left.data);
            if (node.right!=null) q.add(node.right.data);
        }
    }

    public static void main(String[] args) {
        BinaryTree bt = new BinaryTree();

        bt.insert(6);
        bt.insert(2);
        bt.insert(4);
        bt.insert(3);
        bt.insert(8);
        bt.insert(7);
        bt.insert(10);
        bt.insert(1);

        printBFS(bt);
        System.out.println();
        printDFS(bt);
    }
}
