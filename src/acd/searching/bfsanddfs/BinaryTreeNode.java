package acd.searching.bfsanddfs;

public class BinaryTreeNode {
    public Integer data;
    public BinaryTreeNode left;
    public BinaryTreeNode right;

    public BinaryTreeNode(Integer data) {
        this.data = data;
        this.left = null;
        this.right = null;
    }

    public void insert(Integer data){
        if (data <= this.data) {
            if (left == null) {
                left = new BinaryTreeNode(data);
            } else {
                left.insert(data);
            }
        } else {
            if (right == null) {
                right = new BinaryTreeNode(data);
            } else {
                right.insert(data);
            }
        }
    }

    public void printInOrder() {
        if(left != null) left.printInOrder();
        System.out.print(data + " ");
        if (right != null) right.printInOrder();
    }
}
