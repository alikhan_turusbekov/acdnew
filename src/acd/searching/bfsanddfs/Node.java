package acd.searching.bfsanddfs;

public class Node {
    Integer data;
    Node next;

    public Node() {
        this.data=null;
        this.next=null;
    }

    public Node(int data){
        this.data=data;
        this.next=null;
    }
}
