package acd.searching.bfsanddfs;

public class Queue {
    private Node head;
    private Node tail;
    private int size;

    public Queue(){
        this.head = null;
        this.tail = null;
    }

    public void add(Integer data){
        Node n = new Node(data);
        if (head == null) head=n;
        else tail.next=n;
        tail=n;
        size++;
    }

    public Node peak(){
        return this.head;
    }

    public void remove(){
        head=head.next;
        size--;
    }

    public Node poll(){
        Node n = head;
        head=head.next;
        size--;
        return n;
    }

    public int size() {
        return size;
    }

    public boolean isempty() {
        if (this.peak()==null) return true;
        return false;
    }
}
