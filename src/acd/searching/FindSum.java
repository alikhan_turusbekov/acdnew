package acd.searching;

public class FindSum {
    public FindSum(){}

    public boolean isfindtwo(int[] a, int sum){
        Hashtable ht = new Hashtable();
        for (int i=0; i<a.length; i++){
            int n = sum-a[i];
            if(!ht.contains(n)) ht.put(a[i], i+" ");
            else return true;
        }
        return false;
    }

    public void printfindtwo(int[] a, int sum){
        boolean printed=false;
        Hashtable ht = new Hashtable();
        for (int i=0; i<a.length; i++){
            int n = sum-a[i];
            if(!ht.contains(n)) ht.put(a[i], i+" ");
            else {
                System.out.println("Elements are " + a[i] + " and " + n);
                printed=true;
            }
        }
        if (!printed) System.out.println("there are no two elements");
    }

    public boolean isfindelements(int[] a, int sum) {
        Hashtable ht = new Hashtable();
        ht.put(a[0], 0 + " ");
        for (int i = 1; i < a.length; i++) {
            int n = sum - a[i];
            if (!ht.contains(n)) {
                if (!ht.contains(a[i])) ht.put(a[i], i + " ");
                ht.addelementssumwith(a[i],i);
            } else {
                return true;
            }
        }
        return false;
    }

    public String indexesfindelements(int[] a, int sum) {
        Hashtable ht = new Hashtable();
        for (int i = 0; i < a.length; i++) {
            int n = sum - a[i];
            if (!ht.contains(n)) {
                if (!ht.contains(a[i])) ht.put(a[i], i + "");
                ht.addelementssumwith(a[i],i);
            } else {
                return ht.getByKey(n)+" "+i;
            }
        }
        return "";
    }
}
