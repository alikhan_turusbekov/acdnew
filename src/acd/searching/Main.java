package acd.searching;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        FindSum fs = new FindSum();
        Scanner s = new Scanner(System.in);

        int n = s.nextInt();
        int a[] = new int[n];
        for(int i=0; i<n; i++){
           a[i]=s.nextInt();
        }

        int sum = s.nextInt();

        if (fs.isfindtwo(a,sum)) {
            System.out.println("There two elements that sums up properly:");
            fs.printfindtwo(a,sum);
        }
        else System.out.println("There are no two elements that sums up properly");

        if(fs.isfindelements(a,sum)) {
            System.out.println("There is sum of 2 or more numbers:");
            System.out.println(fs.indexesfindelements(a,sum));
        }
        else System.out.println("There no elements summing up");
    }
}
