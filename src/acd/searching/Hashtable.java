package acd.searching;

public class Hashtable {
    public Node[] chain;
    int size;

    public Hashtable() {
        this.size = 20;
        chain = new Node[size];
        for (int i=0; i<size; i++){
            chain[i]= new Node();
        }
    }

    public void put(Integer key, String data){
        int index = key.hashCode() % size;
        boolean check = false;
        Node newnode = new Node(key, data);
        if (chain[index].data==null) chain[index]=newnode;
        else {
            Node node = chain[index];
            while (node.next!=null){
                node = node.next;
                if (key.equals(node.key)) {
                    check = true;
                    break;
                }
            }
            if (check){
                node = newnode;
            } else {
                node.next = newnode;
            }
        }
    }

    public boolean contains(Integer key) {
        int index = key.hashCode() % size;
        if (chain[index].key==key) return true;
        Node node = chain[index];
        while (node.next!=null) {
            node = node.next;
            if (node.key==key) return true;
        }
        return false;
    }

    public void printAll() {
        for (int i=0; i<size; i++){
            System.out.print(i + ": ");
            Node node = chain[i];
            while (node!=null){
                System.out.print(node.data + "->");
                node = node.next;
            }
            System.out.println();
        }
    }

    public void addelementssumwith(Integer data, int i){
        for (int j=0; j<size; j++){
            if (chain[j].data!=null) {
                Node node = chain[j];
                while (node!=null) {
                    if (!this.contains(node.key + data)
                            && node.data.charAt(node.data.length()-1)!=(i+"").charAt(0))
                        this.put(node.key + data, node.data + " " + i);
                    node = node.next;
                }
            }
        }
    }

    public String getByKey(Integer key){
        int index = key.hashCode() % size;
        if (chain[index].key==key) return chain[index].data;
        Node node = chain[index];
        while (node.next!=null) {
            node = node.next;
            if (node.key==key) return node.data;
        }
        return " ";
    }
}
