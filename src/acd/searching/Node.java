package acd.searching;

public class Node {
    Integer key;
    String data;
    Node next;

    public Node(int key, String data){
        this.data=data;
        this.next=null;
        this.key=key;
    }

    public Node(){
        this.key=null;
        this.data=null;
        this.next=null;
    }
}
