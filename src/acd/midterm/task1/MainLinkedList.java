package acd.midterm.task1;

import java.util.Scanner;

public class MainLinkedList {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        LinkedList l = new LinkedList();
        int n = s.nextInt();
        for (int i = 0; i<n ; i++) {   // INPUT O(n);
            int a = s.nextInt();
            l.push_back(a);
        }
        l.print_all(l.head);
    }
}
