package acd.midterm.task1;

public class LinkedList {
    Node head;

    public LinkedList(){
        this.head=null;
    }

    public void push_back(int data){ // It depends on the size of LinkedList, not the size of input meaning it is O(1)
        Node newnode = new Node(data);
        if (head==null) head=newnode;
        else {
            Node point = head;
            while(point.next!=null) {
                point=point.next;
            }
            point.next=newnode;
        }
    }

    public void print_all(Node head){ // O(n) as it goes through all elements
        if (head==null) return;
        Node point = head;
        System.out.print(head.data+" ");
        print_all(point.next);
    }
}
