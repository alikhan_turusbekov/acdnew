package acd.midterm.task2;

import java.util.Scanner;

public class MainStack {
    public static void main(String[] args) {
        Stack s1 = new Stack();
        Stack s2 = new Stack();
        Scanner ss = new Scanner(System.in);
        int n = ss.nextInt();
        for (int i =0 ; i<n; i++) {
            int a = ss.nextInt();
            s1.push(a);
        }
        int m = ss.nextInt();
        for (int i =0 ; i<m; i++) {
            int a = ss.nextInt();
            s2.push(a);
        }
        s1.mergeStacks(s1,s2).removeDuplicates();
        s1.print(s1.head);

    }
}
