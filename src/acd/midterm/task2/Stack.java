package acd.midterm.task2;

public class Stack {
    public Node head;

    public Stack(){
        this.head=null;
    }

    public void print(Node head) {
        if (head==null) return;
        print(head.next);
        System.out.print(head.data+" ");
    }

    public void push(int data){
        Node newnode = new Node(data);
        newnode.next=head;
        head=newnode;
    }

    public int pop(){
        Node pop = head;
        head=head.next;
        return pop.data;
    }

    public Stack mergeStacks(Stack s1, Stack s2){
        Node last = s1.head;
        while (last.next != null) {
            last=last.next;
        }
        last.next=s2.head;
        return s1;
    }

    public void removeDuplicates(){ //O(n*n)
            Node point1 = null, point2 = null, dub = null;
            point1 = head;
            while (point1 != null && point1.next != null) {
                point2 = point1;
                while (point2.next != null) {
                    if (point1.data == point2.next.data) {
                        dub = point2.next;
                        point2.next = point2.next.next;
                        System.gc();
                    } else {
                        point2 = point2.next;
                    }
                }
                point1 = point1.next;
            }
    }
}
