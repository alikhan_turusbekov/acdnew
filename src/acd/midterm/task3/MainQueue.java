package acd.midterm.task3;

import java.util.Scanner;

public class MainQueue {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        Queue q = new Queue();
        int n = s.nextInt();
        for (int i = 0; i < n; i++) { // Input is O(n)
            String ss = s.next();
            q.push(ss);
        }
        q.printWithOdd(q.head);
    }
}
