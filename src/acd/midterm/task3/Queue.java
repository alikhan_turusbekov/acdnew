package acd.midterm.task3;

public class Queue {
    public Node head;
    public Node tail;

    public Queue(){
        this.head=null;
        this.tail=null;
    }

    public void push(String data){ //O(1)
        Node newnode = new Node(data);
        if (head==null) {
            head = newnode;
        } else {
            tail.next = newnode;
        } tail= newnode;
    }

    public String pop(){ // O(1)
        Node pop = head;
        head=head.next;
        return pop.data;
    }

    public void printWithOdd(Node head){ // O(n) as it goes through all of the elements of queue
        if (head==null) return;
        Node point = head;
        if (head.data.length()%2!=0) System.out.print(head.data+" ");
        printWithOdd(point.next);
    }
}
