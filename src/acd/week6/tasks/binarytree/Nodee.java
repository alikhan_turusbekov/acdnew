package acd.week6.tasks.binarytree;

public class Nodee {
    private Integer key;
    private String value;
    private Nodee left;
    private Nodee right;
    public Integer[] sumofeven;

    public Nodee(Integer key, String value) {
        this.key = key;
        this.value = value;
        this.left = null;
        this.right = null;
    }

    public void insert(int key, String value){
        if (key <= this.key) {
            if (left == null) {
                left = new Nodee(key,value);
            } else {
                left.insert(key,value);
            }
        } else {
            if (right == null) {
                right = new Nodee(key,value);
            } else {
                right.insert(key, value);
            }
        }
    }

    public void printInOrder() {
        if(left != null) left.printInOrder();
        System.out.print(value + " ");
        if (right != null) right.printInOrder();
    }

    private void findSumofeven(Integer[] a,int i, int j){
        if (this == null) return;
        a[j] += this.key;
        if (i>j){
            j++;
            if (left!=null) this.left.findSumofeven(a,i,j);
            if (right!=null) this.right.findSumofeven(a,i,j);
        }
    }

    public Integer[] finddd(int i){
        sumofeven = new Integer[i+1];
        for (int q=0; q<i+1; q++){
            sumofeven[q]=0;
        }
        findSumofeven(sumofeven,i+1, 1);
        return sumofeven;
    }

    public void printALLL(int i, int j){
        if (i==j) System.out.print(this.value+" ");
        else if (i>j){
            j++;
            if (left!=null) this.left.printALLL(i,j);
            if (right!=null) this.right.printALLL(i,j);
        }
    }

    public int height(int h){
        int c1=h;
        int c2=h;
        if (left!=null) {
            c1++;
            c1 = left.height(c1);
        }
        if (right!=null) {
            c2++;
            c2 = right.height(c2);
        }
        if (c1>c2) return c1;
        else return c2;
    }

    public Nodee preMostRight(){
        if (this.right!=null) if (this.right.right!=null) return this.right.preMostRight();
        return this;
    }

    public Nodee mostRight(){
        if (this.right!=null) return this.right.mostRight();
        return this;
    }

    public void delete() {
        if (this.right==null  && this.left==null) {
            this.value="";
            this.key=null;
        } else if (this.left!=null) {
            int key = this.left.mostRight().key;
            String value = this.left.mostRight().value;

            if (this.right==null  && this.left==null) {
                this.value="";
                this.key=null;
                // cannot assign to 'this'
            }
            if(this.left.mostRight().left!=null){
                this.left.preMostRight().right=this.left.mostRight().left;
            }else{
                this.left=null;
            }

            this.key=key;
            this.value=value;
        }
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Nodee getLeft() {
        return left;
    }

    public void setLeft(Nodee left) {
        this.left = left;
    }

    public Nodee getRight() {
        return right;
    }

    public void setRight(Nodee right) {
        this.right = right;
    }
}
