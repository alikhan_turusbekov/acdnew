package acd.week6.tasks.binarytree;

public class BSTree {
    public Nodee root;

    public BSTree() {
        this.root = null;
    }

    public void insert(Integer key, String value) {
        if (root == null) root = new Nodee(key,value);
        else root.insert(key, value);
    }

    public void delete(Integer key) {
        if (root.getKey()==key && root.getLeft()==null) root=root.getRight();
        else findNode(root,key).delete();
    }

    public String find(Integer key) {
        Nodee node = findNode(root, key);
        if(node == null) return null;
        else return node.getValue();
    }

    private Nodee findNode(Nodee node, Integer key) {
        if(node == null) return null;
        if(key > node.getKey()) return findNode(node.getRight(), key);
        else if(key < node.getKey()) return findNode(node.getLeft(), key);
        else return node;
    }

    public String findWithoutRecursion(Integer key) {
        Nodee current = root;
        while(current!=null) {
            if(key > current.getKey()) current = current.getRight();
            else if(key < current.getKey()) current = current.getLeft();
            else return current.getValue();
        }
        return null;
    }

    public void printAllAscending() {
        root.printInOrder();
        System.out.println("");
    }

    public int height() {
        return root.height(1);
    }

    public void printSumofeven() {
        int i = height();
        Integer[] n= root.finddd(i);
        for (int j = 0 ; j < i; j++){
            if (j%2==0 && j!=0) System.out.println(n[j]);
        }
    }

    public void printAll() {
        int h = this.height();
        for (int i=1; i<=h; i++){
            root.printALLL(i,1);
        }
    }
}
