package acd.week6.tasks.binarytree;

public class Main {
    public static void main(String[] args) {
        BSTree bsTree = new BSTree();
        BSTree bst = new BSTree();

        bsTree.insert(1000, "A");
        bsTree.insert(2000, "B");
        bsTree.insert(500, "C");
        bsTree.insert(1500, "D");
        bsTree.insert(750, "E");
        bsTree.insert(250, "F");
        bsTree.insert(625, "G");
        bsTree.insert(1250, "H");
        bsTree.insert(875, "I");
        bsTree.insert(810, "Z");

        bsTree.printSumofeven();
    }
}
