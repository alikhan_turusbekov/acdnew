package acd.week6.hashtable;

public class Node extends Object {
    public String key;
    public int value;
    public Node next;

    public Node(String key,int data){
        this.key = key;
        this.value = data;
        this.next = null;
    }

    public Node() {
        this.key = null;
        this.next = null;
    }
}
