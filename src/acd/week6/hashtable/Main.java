package acd.week6.hashtable;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        Hashtable ht = new Hashtable();
        ht.put("Ali",1);
        ht.put("Ami",2);
        ht.put("Han",3);
        ht.put("Pop",4);
        ht.put("Folk",5);
        ht.put("Lopi",6);
        ht.put("Joki",7);
        ht.put("Roki",8);
        ht.put("Folli",9);

        ht.printTable();

        ht.remove("Folli");

        ht.printTable();

        System.out.println(ht.get("Roki"));
    }
}
