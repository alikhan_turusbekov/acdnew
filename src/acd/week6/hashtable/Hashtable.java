package acd.week6.hashtable;

public class Hashtable {
    public Node[] chain;
    private int size;

    public Hashtable(){
        size = 7;
        chain = new Node[size];
        for (int i=0; i<size; i++){
            chain[i]= new Node();
        }
    }

    public void put(String key, int data){
        int i = key.hashCode() % size;
        boolean check = false;
        Node newnode = new Node(key, data);
        if (chain[i]==null) chain[i] = newnode;
        else {
            Node node = chain[i];
            while(node.next!=null){
                node=node.next;
                if (key.equals(node.key)) check = true;
            }
            if (check) {
                node = chain[i];
                while(!key.equals(node.key)){
                    node=node.next;
                }
                node = newnode;
            } else {
                node.next=newnode;
            }

        }
    }

    public void printTable(){
        for (int i = 0; i < this.size; i++) {
            Node node = chain[i];
            System.out.print(i+": ");
            while(node!=null) {
                System.out.print(node.value + "->");
                node = node.next;
            }
            System.out.println();
        }
    }

    public void remove(String key){ //null pointer problem
        int i = key.hashCode()%size;
        Node node = chain[i];
        if (key.equals(chain[i].key)) {
            chain[i]=chain[i].next;
        } else {
            while(!key.equals(node.next.key)){
                node=node.next;
            }
            node.next=node.next.next;
        }
        return;
    }

    public int get(String key){ //null pointer problem
        int number=0;
        int i = key.hashCode()%size;
        Node node = chain[i];
        if (key.equals(chain[i].key)) {
            number = chain[i].value;
        } else {
            while (!key.equals(node.next.key)) {
                node = node.next;
            }
            number = node.next.value;
        }
        return number;
    }

}
