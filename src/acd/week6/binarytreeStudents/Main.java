package acd.week6.binarytreeStudents;

public class Main {
    public static void main(String[] args) {
        BinaryTree bt = new BinaryTree();

        bt.addRoot("Ivan");
        bt.addChild("Ivan", "Rami");
        bt.addChild("Ivan", "Azat");
        bt.addChild("Rami","Tatami");
        bt.addChild("Rami","Kami");
        bt.addChild("Azat", "Adina");
        bt.addChild("Azat", "Apoka");
        bt.addChild("Apoka","Aroga");
        bt.addChild("Apoka", "Arisha");

        bt.print();

        System.out.println(bt.findLevel(""));
    }
}
