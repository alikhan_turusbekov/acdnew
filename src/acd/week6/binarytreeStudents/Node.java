package acd.week6.binarytreeStudents;

public class Node {
    public String s;
    public Node left;
    public Node right;

    public Node(String s){
        this.s=s;
        left=null;
        right=null;
    }

    public Node find(Node s){
        if (this.s == s.s) return this;
        if (this.right!=null) return this.right.find(s);
        if (this.left!=null) return this.left.find(s);
        return null;
    }

    public void addChild(Node child){
        if (this.left==null) this.left=child;
        else if (this.right==null) this.right=child;
        else System.out.println("no child places available");
    }

    public int findL(String s, int i, int j){
        if (i==j) if (this.s==s) return j;
        else if (i>j){
            j++;
            if (left!=null) return this.left.findL(s,i,j);
            if (right!=null) return this.right.findL(s,i,j);
        }
        return 0;
    }

    public int heightTree(int h){
        int c1=h;
        int c2=h;
        if (left!=null) {
            c1++;
            c1 = left.heightTree(c1);
        }
        if (right!=null) {
            c2++;
            c2 = right.heightTree(c2);
        }
        if (c1>c2) return c1;
        else return c2;
    }

    public void printALLL(int i, int j){
        if (i==j) System.out.print(this.s+" ");
        else if (i>j){
            j++;
            if (left!=null) this.left.printALLL(i,j);
            if (right!=null) this.right.printALLL(i,j);
        }
    }

    public void print(){
        System.out.println(this.s);
        if (this.left!=null) left.print();
        if (this.right!=null) right.print();
    }
}
