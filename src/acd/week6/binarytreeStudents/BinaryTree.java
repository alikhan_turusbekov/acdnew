package acd.week6.binarytreeStudents;

public class BinaryTree {
    private Node root;

    public BinaryTree(){
        this.root=null;
    }

    public void addRoot(String s){
        if (root!=null) System.out.println("root was replaced");
        this.root=new Node(s);
    }

    public void addChild(String s, String l){
        Node newnode = new Node(s);
        Node child = new Node(l);
        if (root.find(newnode)!=null) root.find(newnode).addChild(child);
    }

    public Node find(String s){
        Node ss = new Node(s);
        return root.find(ss);
    }

    public int findLevel(String s){
        for (int i=1; i<=heightTree();i++){
            if (root.findL(s,i,1)!=0) return root.findL(s,i,1);
        }
        return 0;
    }

    public int heightSubtree(String s){
        return 0;
    }

    public int heightTree(){
        return root.heightTree(1);
    }

    public void printAll() {
        int h = this.heightTree();
        for (int i=1; i<=h; i++){
            root.printALLL(i,1);
        }
    }

    public void print(){
        root.print();
    }
}
