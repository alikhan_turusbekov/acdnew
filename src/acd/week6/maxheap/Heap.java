package acd.week6.maxheap;

public class Heap {
    public int hsize;
    public Node[] array;

    public Heap(int size){
        array = new Node[size];
        hsize = 0;
    }

    public int getIndexLeft(int i){return 2*i+1;}
    public int getIndexRight(int i){return 2*i+2;}
    public int getIndexParent(int i){return (i-1)/2;}
    public boolean isEmpty(){return (hsize == 0);}

    public int getMax() {
        if (!isEmpty()) return array[0].data;
        return -999999;
    }
}
