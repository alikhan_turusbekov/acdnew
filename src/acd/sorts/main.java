package acd.sorts;

import java.util.Scanner;

public class main {

    public static void myquicksort(int[] a) {
        quicky(a,0,a.length-1);
    }

    private static void quicky(int[] a, int start, int end){
        if (start<end) {
            int j = start - 1;
            for (int i = start; i < end; i++) {
                if (a[i] < a[end]) {
                    j++;
                    int temp = a[j];
                    a[j] = a[i];
                    a[i] = temp;
                }
            }
            j++;
            int temp = a[j];
            a[j] = a[end];
            a[end] = temp;
            quicky(a, start, j-1);
            quicky(a, j+1, end);
        } else return;
    }

    public static void mymergesorting(int[] a){
        mergy(a,0,a.length-1);
    }

    private static void mergy(int[] a, int start, int end){
        if (start<end){
            int middle = (start+end)/2;
            mergy(a, start, middle);
            mergy(a,middle+1, end);

            combine(a,start,end,middle);
        }
    }

    private static void combine(int[] a, int start, int end, int middle){
        int left[] = new int[middle-start+1];
        int right[] = new int[end-middle];

        for(int i=0; i<left.length; i++){
            left[i]=a[start+i];
        }
        for(int j=0; j<right.length; j++){
            right[j]=a[middle+1+j];
        }

        int l=0;
        int r=0;
        int index=start;

        while(l<left.length && r<right.length){
            if (left[l]<right[r]){
                a[index]=left[l];
                l++;
            } else {
                a[index]=right[r];
                r++;
            }
            index++;
        }

        while(l<left.length){
            a[index]=left[l];
            l++;
            index++;
        }

        while(r<right.length){
            a[index]=right[r];
            r++;
            index++;
        }
    }


    private static int[] merge(int[] b, int[] c){
        int a[] = new int[b.length+c.length];
        int bb=0;
        int cc=0;
        for (int i=0; i<a.length; i++){
            if (bb==b.length){
                a[i]=c[cc]; cc++;
            } else if (cc==c.length) {
                a[i]=b[bb]; bb++;
            } else {
                if(b[bb]<=c[cc]) {
                    a[i]=b[bb]; bb++;
                } else {
                    a[i]=c[cc]; cc++;
                }
            }
        }
        return a;
    }

    public static int[] mymergesort(int[] a){
        if (a.length==1) return a;
        int n = a.length / 2;
        int[] left = new int[n];
        int[] right = new int[a.length-n];
        for(int i=0; i<left.length; i++){
            left[i]=a[i];
        }
        for(int j=0; j<right.length; j++){
            right[j]=a[n+j];
        }
        left=mymergesort(left);
        right=mymergesort(right);
        a=merge(left, right);
        return a;
    }

    public static void myinsertionsort(int[] a){
        for (int i=1; i<a.length; i++) {
            for (int j=i-1; j>=0; j--){
                if (a[i] <= a[j]) {
                    int temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                    i=j;
                } else {
                    break;
                }
            }
        }
    }

    public static void myselectionsort(int[] a){
        for (int i=0; i<a.length; i++){
            int min = i;
            for (int j=i; j<a.length; j++){
                if (a[j]<=a[min]) min=j;
            }
            int temp = a[i];
            a[i] = a[min];
            a[min] = temp;
        }
    }

    public static void mybubblesort(int[] a){
        for(int i=0; i<a.length; i++){
            for (int j=0; j<a.length-i-1; j++){
                if (a[j]>a[j+1]){
                    int temp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = temp;
                }
            }
        }
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int[] a = new int[n];
        for (int i=0; i<n; i++){
            a[i]= (int)(Math.random() * 1000000);
        }
        System.out.println("Choose an option:");
        System.out.println("1 — bubble sort");
        System.out.println("2 — selection sort");
        System.out.println("3 — insertion sort");
        System.out.println("4 — merge sort");
        System.out.println("5 — quick sort");
        int option = s.nextInt();
        if (option==1) {
            long time = System.currentTimeMillis();
            mybubblesort(a);
            long time1 = System.currentTimeMillis() - time;
            System.out.println(time1);
        } else if (option==2) {
            long time = System.currentTimeMillis();
            myselectionsort(a);
            long time1 = System.currentTimeMillis() - time;
            System.out.println(time1);
        } else if (option==3) {
            System.out.println("lol");
            long time = System.currentTimeMillis();
            myinsertionsort(a);
            long time1 = System.currentTimeMillis() - time;
            System.out.println(time1);
        } else if (option==4) {
            long time = System.currentTimeMillis();
            mymergesorting(a);
            long time1 = System.currentTimeMillis() - time;
            System.out.println(time1);
        } else if (option==5) {
            long time = System.currentTimeMillis();
            myquicksort(a);
            long time1 = System.currentTimeMillis() - time;
            System.out.println(time1);
        } else {
            System.out.println("Incorrect choice");
        }
        for (int i=0; i<n; i++){
            System.out.print(a[i]+" ");
        }
    }
}
