package acd.week4.queue;

public class QueueTest {
    public static void main(String[] args) {
        Queue q = new Queue();
        q.add("Alikhan");
        q.add("Aloha");
        q.add("Dance");
        q.add("Poping");
        q.add("Polling");

        System.out.println(q.size());

        q.remove();
        q.remove();

        System.out.println(q.size());

        System.out.println(q.poll().data);
        System.out.println(q.peak().data);
    }
}
