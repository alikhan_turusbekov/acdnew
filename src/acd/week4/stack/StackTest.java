package acd.week4.stack;

public class StackTest {
    public static void main(String[] args) {
        Stack stack = new Stack();
        Node top = stack.top();
        stack.push(new Node("Word"));
        stack.push(new Node("Password"));
        stack.push(new Node("Email"));
        stack.push(new Node("Sign in"));

        System.out.println(stack.pop().data);
        System.out.println(stack.pop().data);

        if (stack.empty()) System.out.println("Empty");
        else System.out.println("Not empty");

        System.out.println(stack.size());

    }
}
