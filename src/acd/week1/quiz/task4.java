package kz.aitu.week1.quiz;

import java.util.Scanner;

public class task4 {

    public static boolean palindrome(String word, int i, int j) {
        boolean check = false;
        if (i==j || i>j) return true;
        if (word.charAt(i)==word.charAt(j)) {
            check = check || palindrome(word, i+1, j-1);
        } else check=false;
        return check;
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String word = s.next();
        System.out.println(palindrome(word,0,word.length()-1));
    }
}
