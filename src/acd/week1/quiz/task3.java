package kz.aitu.week1.quiz;

import java.util.Scanner;

public class task3 {

    public static int secondMax(int max, int max2) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        if (n==0) return max2;
        if (n > max) {
            max2=max;
            max=n;
            max2=secondMax(max, max2);
        } else if (n > max2) {
            max2=n;
            max2=secondMax(max, max2);
        }
        return max2;
    }

    public static void main(String[] args) {
        System.out.println(secondMax(0,0));
    }
}