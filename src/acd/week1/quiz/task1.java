package kz.aitu.week1.quiz;

import java.util.Scanner;

public class task1 {

    public static void listBeforeN(int n, int f) {
        if (n==f) System.out.print(f);
        else {
            System.out.print(n+" ");
            listBeforeN(n+1, f);
        }
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();

        listBeforeN(1, n);
    }
}
