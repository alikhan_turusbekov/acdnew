package kz.aitu.week1.quiz;

import java.util.Scanner;

public class task2 {

    public static int findMax() {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        if (n==0) return 0;
        int n2 = findMax();
        if (n2>=n) return n2;
        return n;
    }

    public static void main(String[] args) {
        System.out.println(findMax());
    }
}
