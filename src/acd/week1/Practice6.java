package kz.aitu.week1;

import java.util.Scanner;

public class Practice6 {

    public static int factorize(int a, int n) {
        if (n==1) return a;
        return a*factorize(a,n-1);
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int a = s.nextInt();
        int n = s.nextInt();

        System.out.println(factorize(a, n));
    }
}

