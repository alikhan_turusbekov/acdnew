package kz.aitu.week1;

import java.util.Scanner;

public class Practice5 {

    public static int fibb(int a) {
        if (a==1 || a==0) return 1;
        return fibb(a-1)+fibb(a-2);
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int a = s.nextInt();

        System.out.println(fibb(a-1));
    }
}

