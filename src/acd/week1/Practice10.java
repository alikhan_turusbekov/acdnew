package kz.aitu.week1;

import java.util.Scanner;

public class Practice10 {

    public static int gcd(int n1, int n2) {
        if (n2 == 0) {
            return n1;
        }
        return gcd(n2, n1 % n2);
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n1 = s.nextInt();
        int n2 = s.nextInt();

        System.out.println(gcd(n1, n2));
    }
}

