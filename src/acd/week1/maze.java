package kz.aitu.week1;

import java.util.Scanner;

public class maze {

    int a[][] = new int[5][5];
    public void inputData() {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 5; i ++) {
            for (int j = 0; j < 5; j ++) {
                a[i][j] = scanner.nextInt();
            }
        }
    }

    public void print() {
        for (int i = 0; i < 5; i ++) {
            for (int j = 0; j < 5; j ++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }

    public boolean findPath(int i, int j) {
        if (a[i][j]==0) return false;
        if (((i==4) && (j==4)) && (a[4][4]==1)){
            a[i][j] = 2;
            return true;
        }
        a[i][j] = 0;

        boolean flag = false;
        if (0<=i && i<4) if (findPath(i+1,j)) flag=true;
        if (1<=i && i<=4) if (findPath(i-1,j)) flag=true;
        if (0<=j && j<4) if (findPath(i,j+1)) flag=true;
        if (1<=j && j<=4) if (findPath(i,j-1)) flag=true;

        if (flag == true) a[i][j]=2;
        return flag;

    }

    public void run() {
        inputData();
        System.out.println(findPath(0, 0));
        print();
    }
}
