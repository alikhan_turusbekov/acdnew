package kz.aitu.week1;

import java.util.Scanner;

public class Practice8 {

    public static int biCoef(int n, int k) {
        if (k==0 || k==n) return 1;
        return biCoef(n-1,k-1)+biCoef(n-1, k);
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int k = s.nextInt();
        System.out.println(biCoef(n,k));
    }
}
