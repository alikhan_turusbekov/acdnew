package kz.aitu.week2;

import java.util.Scanner;

public class Week2707 {

    public int factorial(int n) {
        if(n==1) return 1;
        return n*factorial(n-1);
    }

    public static void permutation(char a[], int i, int n) {
        if(n==0) return;
        if(i+1==a.length) {
            i=0;
        }
        char b=a[i];
        a[i]=a[i+1];
        a[i+1]=b;
        for(int j=0;j<a.length;j++) {
            System.out.print(a[j]);
        }
        System.out.println();
        permutation(a, i+1, n-1);
    }

    public void run() {
        Scanner sc = new Scanner(System.in);
        String one = sc.next();
        permutation(one.toCharArray(), 0, factorial(one.length()));
    }
}
