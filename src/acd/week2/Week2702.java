package kz.aitu.week2;

import java.util.Scanner;

public class Week2702 {

    public void reverse(int n){
        Scanner ss = new Scanner(System.in);
        int k = ss.nextInt();
        if (n==1) {
            System.out.print(k+" ");
        }
        else {
            reverse(n - 1);
            System.out.print(k+" ");
        }
    }

    public void run() {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        reverse(n);
    }
}
