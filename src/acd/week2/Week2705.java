package kz.aitu.week2;

import java.util.Scanner;

public class Week2705 {
    public void permutation(int n,int k, int a[]) {
        if(n==6) {
            for(a[0]=1;a[0]<=k;a[0]++) {
                permutation(n-1,k,a);
            }
        }
        if(n==5) {
            for(a[1]=1;a[1]<=k;a[1]++) {
                permutation(n-1,k,a);
            }
        }
        if(n==4) {
            for(a[2]=1;a[2]<=k;a[2]++) {
                permutation(n-1,k,a);
            }
        }
        if(n==3) {
            for(a[3]=1;a[3]<=k;a[3]++) {
                permutation(n-1,k,a);
            }
        }
        if(n==2) {
            for(a[4]=1;a[4]<=k;a[4]++) {
                permutation(n-1,k,a);
            }
        }
        if(n==1) {
            for(a[5]=1;a[5]<=k;a[5]++) {
                for(int i=0;i<6;i++) {
                    if(a[i]>0) {
                        System.out.print(a[i]+" ");
                    }
                }
                System.out.println();
            }
        }
    }

    public void run() {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int k = s.nextInt();

        int a[]=new int[6];
        for (int i=0; i<6; i++) a[i]=0;
        permutation(n,k,a);

    }
}
