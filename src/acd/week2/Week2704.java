package kz.aitu.week2;

import java.util.Scanner;

public class Week2704 {

    public void spiral(int n,int a[][],int p,int c,int b) {
        int i;
        if(c==p){
            a[p][c]=b;
            b++;
        }
        if(b<=n*n && c>p) {
            for(i=p;i<=c;i++) {
                a[p][i]=b;
                b++;
            }
            for(i=p+1;i<=c;i++) {
                a[i][c]=b;
                b++;
            }
            for(i=c-1;i>=p;i--) {
                a[c][i]=b;
                b++;
            }
            for(i=c-1;i>p;i--) {
                a[i][p]=b;
                b++;
            }
            spiral(n,a,p+1,c-1,b);
        }  else {
            for(int k=0;k<n;k++) {
                for(int j=0;j<n;j++) {
                    System.out.print(a[k][j]+" ");
                }
                System.out.println();
            }
        }
    }

    public void run() {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int a[][] = new int[100][100];
        for (int i=0; i<100;i++) {
            for (int j=0; j<100; j++) {
                a[i][j]=0;
            }
        }

        spiral(n, a,0, n-1, 1);
    }
}
