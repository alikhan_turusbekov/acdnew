package kz.aitu.week2;

import java.util.Scanner;

public class Week2708 {

    public int room(int n, int i, int j, char a[][],int count) {
        if (a[i][j]=='*') return 0;
        count++;
        a[i][j]='*';
        if (i+1!=n) count+=room(n,i+1,j,a,0);
        if (i!=0) count+=room(n,i-1,j,a,0);
        if (j+1!=n) count+=room(n,i,j+1,a,0);
        if (j!=0) count+=room(n,i,j-1,a,0);
        return count;
    }

    public void run() {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        char a[][] = new char[n][n];

        for (int i=0; i<n; i++) {
            for (int j=0; j<n; j++) {
                a[i][j] = s.next().charAt(0);
            }
        }

        int i = s.nextInt();
        int j = s.nextInt();
        System.out.println(room(n,i-1,j-1,a,0));
    }
}
