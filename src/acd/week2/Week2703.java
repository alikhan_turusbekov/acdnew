package kz.aitu.week2;

import java.util.Scanner;

public class Week2703 {

    public void reverse(int n) {
        Scanner ss = new Scanner(System.in);
        String word = ss.nextLine();
        if (n == 1) {
            System.out.println(word);
        } else {
            reverse(n-1);
            System.out.println(word);
        }

    }

    public void run() {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        reverse(n);
    }
}
