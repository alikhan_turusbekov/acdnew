package kz.aitu.week2;

import java.util.Scanner;

public class Week2701 {

    public int fibonacci(int n) {
        if (n==1 || n==0) return n;
        return fibonacci(n-1)+fibonacci(n-2);
    }

    public void run() {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        System.out.println(fibonacci(n));
    }
}
