package kz.aitu.week2;

import java.util.Scanner;

public class Week2706 {
    char a[][] = new char[100][100];
    public void inputData(int n, int m) {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < n; i ++) {
            for (int j = 0; j < m; j ++) {
                a[i][j] = scanner.next().charAt(0);
            }
        }
    }

    public boolean findPath(int i, int j, int n, int m) {
        if (a[i][j]=='#') return false;
        if (i == n-1 && j == m-1 && a[n-1][j-1]=='.') return true;
        a[i][j] = '#';

        boolean flag = false;
        if (i+1!=n) flag = flag || (findPath(i+1,j,n,m));
        if (i!=0) flag = flag || (findPath(i-1,j,n,m));
        if (j+1!=m) flag = flag || (findPath(i,j+1,n,m));
        if (j!=0) flag = flag || (findPath(i,j-1,n,m));
        return flag;
    }

    public void run() {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int m = s.nextInt();

        inputData(n,m);
        System.out.println(findPath(0, 0, n, m));
    }
}
