package acd.graph;

public class Stack<Vertex> {
    private Node<Vertex> top;
    private int size = 0;

    public Stack(){
        this.top = new Node<>();
    }

    public Node top(){
        return top;
    }

    public void push(Vertex data){
        Node newnode = new Node<>(data);
        newnode.next = top;
        top = newnode;
        size++;
    }

    public Node pop(){
        Node n = top;
        top=top.next;
        size--;
        return n;
    }

    public int size(){
        return size;
    }

    public boolean empty(){
        if (top==null) return true;
        return false;
    }
}
