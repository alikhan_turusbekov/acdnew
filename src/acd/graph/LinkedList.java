package acd.graph;

public class LinkedList<Vertex>{
    public Node<Vertex> head;
    public Node<Vertex> tail;

    public LinkedList(){
        this.head = null;
        this.tail = null;
    }

    public Node<Vertex> head(){
        return head;
    }

    public Vertex pop(){
        Node<Vertex> node = head;
        head=head.next;
        return node.vertex;
    }

    public void add(Vertex v){
        Node<Vertex> node = new Node<>(v);
        if (head==null) head = node;
        if (tail!=null) {
            tail.next = node;
            tail=tail.next;
        } else tail = node;
    }

    public boolean contains(Vertex v){
        Node<Vertex> current=this.head;
        if (current==null) return false;
        while (current.next!=null) {
            if (current.vertex==v) return true;
            current=current.next;
        }
        if (current.vertex==v) return true;
        return false;
    }

    public void remove(Vertex v){
        if (!this.contains(v)) return;
        Node<Vertex> current = head;
        if (current.vertex==v){
            head=current.next;
            return;
        }
        while (current.next!=null) {
            if (current.next.vertex==v) {
                current.next=current.next.next;
                return;
            };
            current=current.next;
        }
    }
}
