package acd.graph;

public class Vertex<Key,Value> {
    public Key key;
    public Value value;
    public LinkedList<Vertex<Key, Value>> edges;
    public Vertex<Key,Value> next;

    public Vertex(Key key, Value value){
        this.key = key;
        this.value = value;
        this.edges = new LinkedList();
        this.next = null;
    }

    public Vertex() {
        this.key = null;
        this.value = null;
        this.next = null;
        this.edges = new LinkedList();
    }

    public void addEdge(Vertex<Key, Value> v){
        if (!edges.contains(v)) edges.add(v);
    }

    public void removeEdge(Vertex<Key, Value> v){
        if (edges.contains(v)) edges.remove(v);
    }

    public void remove(Vertex<Key,Value> v){
        if (edges.contains(v)) edges.remove(v);
    }
}
