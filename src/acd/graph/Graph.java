package acd.graph;

public class Graph<Key, Value> {
    public Hashtable<Key, Value> hashtable;
    public boolean bidirectional = true;
    public Integer edges=0;

    public Graph(){
        hashtable = new Hashtable<Key, Value>();
    }

    public void setDirection(boolean d){
        bidirectional = d;
    }

    public void addVertex(Key key, Value value){
        hashtable.put(key, value);
    }

    public void addEdge(Key k1, Key k2){
        edges++;
        Vertex<Key,Value> v1 = hashtable.get(k1);
        Vertex<Key,Value> v2 = hashtable.get(k2);

        v1.addEdge(v2);
        if (bidirectional) v2.addEdge(v1);
    }

    public Integer countEdges(){
        return edges;
    }

    public void removeVertex(Key key){
        if (bidirectional) {
            Vertex<Key,Value> vertex = hashtable.get(key);
            while (vertex.edges.head!=null){
                Vertex<Key, Value> v= vertex.edges.pop();
                if (v.edges.contains(vertex)) removeEdge(v.key, vertex.key);
            }
        }

        if (hashtable.contains(key)) hashtable.remove(key);
    }

    public void removeEdge(Key k1, Key k2){
        edges--;
        Vertex<Key,Value> v1 = hashtable.get(k1);
        Vertex<Key,Value> v2 = hashtable.get(k2);

        v1.removeEdge(v2);
        if (bidirectional) v2.removeEdge(v1);
    }

    public void printAll(){
        hashtable.printAll();
    }

    public void BFSprint(Key key1, Key key2){
        LinkedList<Vertex<Key, Value>> marked = new LinkedList<>();
        Vertex<Key, Value> v1 = hashtable.get(key1);
        Vertex<Key, Value> v2 = hashtable.get(key2);
        if (v1==v2) return;
        Queue<Vertex<Key,Value>> q = new Queue<>();
        q.add(v1);
        marked.add(v1);
        while(!q.isempty()){
            Node<Vertex<Key, Value>> node = q.poll();
            System.out.print(node.vertex.value+" ");
            if (node.vertex.edges.head==null) return;
            Node<Vertex<Key, Value>> current = node.vertex.edges.head;
            while(current.next!=null){
                if (!marked.contains(current.vertex)) {
                    if (current.vertex == v2) return;
                    q.add(current.vertex);
                    current = current.next;
                }
            }
            q.add(current.vertex);
        }
    }
}
