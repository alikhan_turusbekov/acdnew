package acd.graph;

public class Hashtable<Key, Value> {
    public Vertex<Key, Value>[] chain;
    int size;

    public Hashtable() {
        this.size = 20;
        chain = new Vertex[size];
        for (int i=0; i<size; i++){
            chain[i]= new Vertex<>();
        }
    }

    public void put(Key key, Value value){
        Vertex<Key, Value> newvertex = new Vertex<>(key, value);
        int index = key.hashCode() % size;
        boolean check = false;
        if (chain[index].key==null) chain[index]=newvertex;
        else {
            Vertex<Key,Value> current = chain[index];
            while (current.next!=null){
                current = current.next;
                if (newvertex.key.equals(current.key)) {
                    check = true;
                    break;
                }
            }
            if(check){
                current = newvertex;
            } else {
                current.next = newvertex;
            }
        }
    }

    public void remove(Key key){
        if (!contains(key)) return;
        int i = key.hashCode()%size;
        Vertex<Key,Value> current = chain[i];
        if (key.equals(current.key)) {
            chain[i]=chain[i].next;
        } else {
            while(!key.equals(current.next.key)){
                current=current.next;
            }
            current.next=current.next.next;
        }
        return;
    }

    public boolean contains(Key key){
        int i = key.hashCode() % size;
        if (chain[i].key==null) return false;
        Vertex<Key,Value> current = chain[i];
        if (current.key.equals(key)) {
            return true;
        } else {
            while(current.next!=null){
                if (current.key.equals(key)) return true;
                current=current.next;
            }
            if(current.key.equals(key)) return true;
        }
        return false;
    }

    public Vertex<Key, Value> get(Key key){
        Vertex<Key, Value> vertex = null;
        int i = key.hashCode() % size;
        Vertex<Key,Value> current = chain[i];
        if (current == null) return null;
        if (key.equals(current.key)) {
            vertex = current;
        } else {
            while (!key.equals(current.next.key)) {
                current = current.next;
            }
            vertex = current.next;
        }
        return vertex;
    }

    public void printAll(){
        for (int i=0; i<size; i++){
            if (chain[i]==null) {}
            else {
                Vertex<Key,Value> current = chain[i];
                while(current.next!=null){
                        if (current.value!=null) System.out.print(current.value+" ");
                        current=current.next;
                }
                if (current.value!=null) System.out.print(current.value+" ");
            }
        }
        System.out.println();
    }

}
