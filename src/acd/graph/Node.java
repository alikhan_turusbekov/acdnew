package acd.graph;

public class Node<Vertex> {
    public Vertex vertex;
    public Node<Vertex> next;

    public Node(){
        this.vertex=null;
        this.next=null;
    }

    public Node(Vertex vertex){
        this.vertex=vertex;
        this.next=null;
    }
}
