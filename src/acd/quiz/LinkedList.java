package acd.quiz;

public class LinkedList{ //nothing to change
    private Node head;
    private Node tail;

    public LinkedList(){
        this.head = new Node("head");
        tail = head;
    }

    public Node head(){
        return head;
    }

    public void add(Node node){
        tail.next = node;
        tail = node;
    }

    public void insertAt(String s, int index) {
        Node newnode = new Node(s);
        Node pointer = head;
        for(int i=1; i<index; i++){
            pointer=pointer.next;
        }
        newnode.next=pointer.next.next;
        pointer.next=newnode;
    }

    public void removeAt(int index) {
        if (index == 0) {
            head=head.next;
        } else {
            Node pointer = head;
            for(int i=1; i<index; i++){
                pointer=pointer.next;
            }
            pointer.next=pointer.next.next;
        }
    }

    public void print(Node head2) {
        if (head2.next==null) {
            System.out.println(head2.data);
            return;
        }
        System.out.print(head2.data+" ");
        print(head2.next);
        // just to see
    }
}
