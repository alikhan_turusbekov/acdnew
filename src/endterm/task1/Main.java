package endterm.task1;

public class Main {
    public static void main(String[] args) {
        BinaryTree bt = new BinaryTree();

        bt.add(5);
        bt.add(7);
        bt.add(4);
        bt.add(8);
        bt.add(3);
        bt.add(1);
        bt.add(6);

        System.out.println(bt.getHeight(bt.root)); // task 1

        System.out.println(bt.totalSum(bt.root)); // task 2

        System.out.println(bt.findMax()); // task 3
    }
}
