package endterm.task1;

public class Nodee {
    public Integer data;
    public Nodee next;

    public Nodee(){
        this.data=null;
        this.next=null;
    }

    public Nodee(Integer data){
        this.data=data;
        this.next=null;
    }
}
