package endterm.task1;

public class Queue {
    public Nodee head;
    public Nodee tail;

    public Queue(){
        this.head=null;
        this.tail=null;
    }

    public void add(Integer data){
        Nodee n = new Nodee(data);
        if (head==null) head=n;
        if (tail==null) tail=n;
        tail.next=n;
        tail=tail.next;
    }

    public int pop(){
        int n = head.data;
        head = head.next;
        return n;
    }

    public boolean isempty(){
        if (head==null) return true;
        return false;
    }
}
