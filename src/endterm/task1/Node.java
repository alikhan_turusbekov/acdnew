package endterm.task1;

public class Node {
    public Integer data;
    public Node left;
    public Node right;

    public Node(){
        this.data=null;
        this.left=null;
        this.right=null;
    }

    public Node(Integer data){
        this.data=data;
        this.left=null;
        this.right=null;
    }

    public void insert(Integer data){
        if (data <= this.data) {
            if (left == null) {
                left = new Node(data);
            } else {
                left.insert(data);
            }
        } else {
            if (right == null) {
                right = new Node(data);
            } else {
                right.insert(data);
            }
        }
    }

    public void print(){
        if (this.left!=null) this.left.print();
        System.out.print(this.data+" ");
        if (this.right!=null) this.right.print();
    }

    public int height(int height){
        if (this.left==null && this.right==null) return height;
        int lefth=height;
        int righth=height;
        if (this.left!=null) {
            lefth++;
            lefth = this.left.height(lefth);
        }
        if (this.right!=null) {
            righth++;
            righth = this.right.height(righth);
        }
        if (lefth>righth) return lefth;
        return righth;
    }

    public Node find(Integer data){
        Node node = this;
        if (data < node.data) {
            node = left.find(data);
        } else if (data > node.data) {
            node = right.find(data);
        }
        return node;
    }
}
