package endterm.task1;

public class BinaryTree {
    public Node root;

    public BinaryTree(){
        root=null;
    };

    public void add(Integer data){
        if (root==null) root=new Node(data);
        else root.insert(data);
    }

    public void print(){
        root.print();
    }

    public int getHeight(Node node){
        if (node.right==null && node.left==null) return 1;
        return node.height(1);
    }

    public int totalSum(Node node){
        int sum=0;
        Queue q = new Queue();
        q.add(node.data);
        while(!q.isempty()){
            int n =q.pop();
            sum += n;
            Node nodee = node.find(n);
            if (nodee.right!=null) q.add(nodee.right.data);
            if (nodee.left!=null) q.add(nodee.left.data);
        }
        return sum;
    }

    public int findMax(){
        Node node = root;
        while (node.right!=null){
            node=node.right;
        }
        return node.data;
    }
}
