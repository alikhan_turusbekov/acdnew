package endterm.task2;

import javax.swing.*;
import java.util.Scanner;

public class Main {
    public static boolean checkStrings(String s1, String s2){
        if (s1.length()!=s2.length()) return false;
        char[] ss1 = new char[s1.length()];
        for (int i=0; i<s1.length(); i++) {
            ss1[i]=s1.charAt(i);
        }
        char[] ss2 = new char[s2.length()];
        for (int i=0; i<s2.length(); i++) {
            ss2[i]=s2.charAt(i);
        }
        quicky(ss1);
        quicky(ss2);
        boolean check = true;
        for (int i=0; i<ss1.length;i++){
            if (ss1[i]!=ss2[i]) check=false;
        }
        return check;
    }

    public static void quicky(char[] a){
        myquicky(a, 0, a.length-1);
    }

    private static void myquicky(char[] a, int start, int end){
        if (start < end){
            int j = start - 1;
            for (int i=start; i<end; i++){
                if (a[i]<a[end]){
                    j++;
                    char temp = a[i];
                    a[i]=a[j];
                    a[j]=temp;
                }
            }
            j++;
            char temp = a[j];
            a[j]=a[end];
            a[end]=temp;
            myquicky(a, start, j-1);
            myquicky(a,j+1, end);
        }
    }

    public static int findIndex(int[] a, int f){
        int index=0;
        for (int i=0; i<a.length; i++){
            if (a[i]==f) index = i;
        }
        return index;
    }

    public static void main(String[] args) {
        String s1 = "anagram";
        String s2 = "nagaram";

        if (checkStrings(s1,s2)) System.out.println("true");
        else System.out.println("false");  // task4

        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int a[] = new int[n];
        for (int i =0; i<n; i++){
            a[i]= s.nextInt();
        }
        int f = s.nextInt();
        System.out.println(findIndex(a,f));
    }
}
